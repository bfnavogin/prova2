package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;
    
    public Ponto(){
       
    }
    
    public Ponto(double x, y, z){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    public void setPontox(double x){
        this.x = x;
    }
    public void setPontoy(double y){
        this.y = y;
    }
    public void setPontoz(double z){
        this.z = z;
    }
    public Double getPontox(){
        return x;
    }
    public Double getPontoy(){
        return y;
    }
    public Double getPontoz(){
        return z;
    }
    
    public Double dist(Ponto){
        public double d;
        
        d = Math.sqrt((Math.pow(x2-x1, 2)) + (Math.pow(y2-y1, 2)) + (Math.pow(z2-z1, 2)));
        return d;
    }

    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

}
